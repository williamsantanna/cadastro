require 'spec_helper'

describe "Show a product" do
  it "Should show a product" do
    product = Product.create :name => "cachaca", :description => "bom", :weight => "1000"
    visit products_path
    click_link "Show"
    # save_and_open_page
    page.should have_content "cachaca"
  end
end