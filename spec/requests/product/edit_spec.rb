require 'spec_helper'

describe "Edit a product" do

  it "Should edit a product" do
    product = Product.create name:"cachaca", description:"ruim", weight:"500"
    visit products_path
    click_link "Edit"
    fill_in "Name", with: "licor"
    fill_in "Description", with: "bom"
    fill_in "Weight", with: "100"
    # save_and_open_page
    click_button "Update Product"
    page.should have_content "licor"
    # save_and_open_page
  end
end

