require 'spec_helper'

describe 'Create a product' do
  it 'Should create a product' do
    visit new_product_path
    fill_in "Name", with: "vinho"
    fill_in "Description", with: "ruim"
    fill_in "Weight", with: "800"
    click_button "Create Product"
    page.should have_content "vinho"
  end
end