require 'spec_helper'

describe "Destroy a product" do
  let!(:product){Product.create :name => "mate", :description => "otimo", :weight => "500"}
  it "Should destroy a product" do
    # product = Product.create :name => "refrigerante", :description => "bom", :weight => "390"
    visit products_path
      click_link "Destroy"
      # page.should have_no_content "refrigerante"
      page.should have_no_content "mate"
      page.should have_content "Product was successfully destroyed."
      # save_and_open_page
  end
end