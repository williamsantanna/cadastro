require 'spec_helper'

describe "Edit a person" do
  it "should edit a person" do
    person = Person.create name:"maria", birthdate:'08/12/1950', height:'1.70'
     visit people_path
      click_link "Edit"
      fill_in "Name",  with: "victor"
      fill_in "Birthdate", with: '11/04/1954'
      fill_in "Height", with: '1.75'
      click_button "Update Person"
      # save_and_open_page
      page.should have_content "victor"
  end
end