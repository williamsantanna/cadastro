require 'spec_helper'

describe 'show a new person' do
  it 'should show a person' do
     person = Person.create :name => "william", :birthdate => '11/06/1979', :height => '1.80'
     visit people_path
     click_link "Show"
     page.should have_content "william"
     # save_and_open_page
  end
end