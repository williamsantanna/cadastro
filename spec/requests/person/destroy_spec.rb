require 'spec_helper'

describe  'destroy a person' do
  # Executa o bloco criando a pessoa antes de executar o teste: "let!(:person)Person.create :name => "wesley", :birthdate => '15/03/1978', :height => '1.80'"
  let!(:person){Person.create :name => "william", :birthdate => '11/06/1979', :height => '1.80'}
  it 'should destroy a person' do
  # pois para cada usuario ele renderiza no index.html.haml linha 10, caso nao tenha um usuario abre-se a tela sem os links
  # criar o usuario
    visit people_path   #redireciona para o index
    click_link "Destroy"
    page.should have_no_content "william"
    page.should have_content "Person was successfully destroyed."
    # save_and_open_page
  end
end