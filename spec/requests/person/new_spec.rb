require 'spec_helper'

describe 'creation of user' do
    it 'should create a new user' do
        visit new_person_path
        within 'form' do
            fill_in "Name", with: "will"
            fill_in "Birthdate", with: '24/08/1984'
            fill_in "Height", with: "1.90"
        end
        click_button "Create Person"

        page.should have_content "will"
        # save_and_open_page
    end
end
