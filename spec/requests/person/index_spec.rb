require 'spec_helper'

describe 'list of people' do #, :js => "true"  do

  #Este teste verifica a criacao do usuario primeiro e verifica se foi criado
  it 'Should list all people' do
    # cria uma pessoa no banco com os atributos que estão sendo passados
    person = Person.create :name => "will", :birthdate => '24/08/1984', :height => '1.90'

    visit people_path

    page.should have_content "will"

    # save_and_open_page

  end
end