require 'spec_helper'

describe 'sign out' do
  it 'should sign out' do
    # fazer login, implica em criar um usuário na fase de testes
    user = create_user_login('will', '123')
    sign_in user.user, user.password
    visit users_path
    click_link "Sign Out"
    page.should have_no_css '#session_id', :text => user.user
  end
end
