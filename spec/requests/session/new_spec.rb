require 'spec_helper'

# describe 'sign in', :js => true do
describe 'sign in' do

    it 'should access with a valid user' do
        user = create_user_login('william', '123')
        # entra com o usuário e a senha padrão
        sign_in user.user, user.password
        page.should have_css "#session_id", {:text => user.user}
        save_and_open_page
    end

    # # teste de validação
    # it 'should try access with a invalid user' do
    #     create_user_login 'will', '123'
    #     sign_in 'rplaurindo', '123'
    #     page.should have_content 'Invalid user or password!'
    #     save_and_open_page
    # end

    # it 'should try access with a invalid password' do
    #     create_user_login 'will', '123'
    #     sign_in 'will', '321'
    #     save_and_open_page
    # end
end
