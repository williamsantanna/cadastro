require 'spec_helper'

describe 'Edit a user' do
  let!(:user){User.create user: "maria", password: "123"}
  it 'Should edit a user' do
    visit edit_user_path user
    within '#form_1' do
      fill_in "User", with: "victor"
      fill_in "Password", with: "321"
    end
    click_button "Update User"
    page.should have_content "victor"
  end
end