require 'spec_helper'

# não usar bibliotecas nesse, para que sirva de exempĺo de como fica mais verboso
# além de ver passo a passo como funcionam o testes

describe 'Creation of user' do
  # habilitando o js, o teste eh mostrado no browser
  it 'Should create a new user' do
    # Visualiza-se com o rake routes. O new, vem por default no rails, para a criação de um novo, usuário, no caso.
    # Obs.: O _path é obrigatório para o rails.
    visit new_user_path
    # o capybara, atraves do within, que usa a sintaxe de blocos, procura atraves de ids, otimizando o teste.
    within '#form_1' do
    #     # Obs.: O rails, por default, cria o label com letras maiúsculas
    #     # insere texto de acordo com uma label, id ou name
        fill_in "User", with: "will"
        fill_in "Password", with: "123"
    end
    # save_and_open_page
    # # clica no botão
    click_button "Create User"

     # save_and_open_page
    # # verifica, no html, a string passada como argumento.
    page.should have_content "will"
  end
end