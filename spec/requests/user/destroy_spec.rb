require 'spec_helper'

describe 'Destroy a user' do
  let!(:user){User.create user:"jose", password:"321"}
  it 'Should destroy a user' do
    visit users_path
    click_link "Destroy"

    page.should have_no_content "jose"

    page.should have_content "User was successfully destroyed."
  end
end