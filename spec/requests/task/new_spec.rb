require 'spec_helper'

describe 'Create new task' do
  it 'Should create new task' do
    visit new_task_path
    within 'form' do
      fill_in 'Name', with: "POG"
      fill_in 'Description', with: "POG"
      fill_in "Date", with: '03/09/2001'
    end
    click_button "Create Task"
    page.should have_content "POG"
  end
end