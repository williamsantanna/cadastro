require 'spec_helper'

describe 'Show a task' do
  it 'Should show a task' do
    task = Task.create :name => "wil", :date => '11/06/1979', :description => "POG"
    visit tasks_path
    page.should have_content "wil"
  end
end

