require 'spec_helper'

describe 'List tasks per page' do

  # it 'Should list all tasks' do
  #   @task = Task.create :name => "POG", :date => "03/09/2001", :description => "Manter a ordem publica"
  #   visit tasks_path
  #   page.should have_content "POG"
  #   save_and_open_page
  # end

  it 'Should list five tasks per page' do
    for @task in 0..9
    @task = Fabricate(:task, score: 10, name: "fuck")
    end
    # @task = Task.create :name => "bope"
    # @task = Task.create :name => "cavalaria"
    # @task = Task.create :name => "rancho"
    # @task = Task.create :name => "p1"
    # @task = Task.create :name => "p2"
    # @task = Task.create :name => "p3"
    # @task = Task.create :name => "p4"
    # @task = Task.create :name => "p5"
    visit tasks_path
    # save_and_open_page
    page.should have_content @task.name #verifica se o conteudo existe na primeira pagina antes do "next".
    click_link "Next"
    page.should have_content 'choque' #verifica se o conteudo existe na segunda pagina apos "next".
    save_and_open_page
  end

  # it 'Should list the last tasks in page' do
  #   @task = Task.create :name => "choque"
  #   @task = Task.create :name => "bope"
  #   visit tasks_path
  #   page.should have_content 'choque'
  #   page.should have_content 'bope'
  #   page.should have_no_content 'Next'
  #   save_and_open_page
  # end
end