require 'spec_helper'

describe 'Destroy a task' do
  let!(:task){Task.create :name => "AREP3", :date => "03/09/2001", :description => "Manter a ordem publica"}
  it 'Should destroy a task' do
    visit tasks_path
    click_link "Destroy"
    page.should have_no_content "AREP3"
    save_and_open_page
    page.should have_content "Task was successfully destroyed."
  end
end