require 'spec_helper'

describe  'Edit a task' do
  it 'Should edit a task' do
    task = Task.create name:"SETOR", date:'03/09/2001', description:"Manter a ordem publica"
    visit tasks_path
    click_link "Edit"
    fill_in "Name", with: "POG"
    fill_in "Date", with: "18/03/2001"
    fill_in "Description", with: "Policiamento Ostensivo Geral"
    click_button "Update Task"
    page.should have_content "POG"

  end
end