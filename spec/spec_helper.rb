# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}


#criado posteriormente
module Fill

  # sintax: moduleName_methodName
  def fill_login(login, pwd, form_id="#form_1")
    within form_id do
      fill_in "User", with: login
      fill_in "Password", with: pwd
    end
  end
end

module Create

  def create_user_login(login, password)
    User.create({:password => password, :user => login})
  end

  # não é uma boa prática misturar métodos do rspec com métodos do Ruby
  def sign_in (login, password)
    visit new_sessions_path
    fill_login(login, password)
    click_button "Sign In"
  end

end


RSpec.configure do |config|

  #inclui o modulo
  config.include Fill
  config.include Create

  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    sleep(1)
    DatabaseCleaner.clean
    #DatabaseCleaner.reset_database
  end
end
