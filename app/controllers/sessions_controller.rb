class SessionsController < ApplicationController

  def new
    @session = User.new
  end

  def create
    @session = User.find_by_user(params[:user][:user])

    if @session && (@session.password == params[:user][:password])

      session[:user_id] = @session.id

      redirect_to users_path, {:notice => "#{@session.user} was logged with success!"}

    else
      redirect_to new_sessions_path, {:notice => "Invalid user or password!"}
    end

  end

  def destroy
    session[:user_id] = nil
    redirect_to users_path
  end


end