class ProductsController < ApplicationController

  respond_to :html, :js

  def new
    @product = Product.new
  end

  def index
    @products = Product.all
    @product = Product.new

    respond_to do |format|
      format.html
    end
  end

  def create
    @product = Product.new(params[:product])
    @product.save

    respond_with @product
  end

  def show
    @product = Product.find(params[:id])
  end

  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    respond_with @product
  end

  def edit
    # @product = Product.find(params[:id])
    respond_with @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])

    respond_to do |format|
      if @product.update_attributes(params[:product])
         format.html {redirect_to product_path}
      else
         format.html {render action: "edit"}
      end
    end
  end

  #metodo que sera chamado na partial _list para mapear os dados do banco.
  def map_products
    Product.all #.all e um metodo do aplication_controller que retorna todos os registros do banco.
  end
  helper_method :map_products #permite que o metodo map_products seja acessado de fora da classe.

end