class TasksController < ApplicationController
  # load_and_authorize_resource
  respond_to :html, :js, :xml, :json

  def new
    @task = Task.new
  end

  def create
    respond_with @task = Task.create(params[:task])
  end

  def show
    @task = Task.find(params[:id])
  end

  def index
    respond_with @tasks = Task.order("name").page(params[:page])
  end

  def destroy
    @task = Task.destroy(params[:id])

    if map_tasks.count != 0
      respond_with @task, :location => tasks_path(:page => params[:page])
    else
      respond_with @task, :location => tasks_path(:page => params[:page]) # para terminar
    end
  end

  def edit
    @task = Task.find(params[:id])
  end

  def update
    respond_with @task = Task.update(params[:id], params[:task])
  end

   #metodo que sera chamado na partial _list para mapear os dados do banco.
  def map_tasks
    Task.order("name").page(params[:page]) #Sets the number of elements per page, order by name.
  end
  helper_method :map_tasks #permite que o metodo map_products seja acessado de fora da classe.

end