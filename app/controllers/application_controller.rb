class ApplicationController < ActionController::Base

  protect_from_forgery

    # disponibiliza o método publicamente
  def current_user
    User.find(session[:user_id]) if session[:user_id]
  end

  # rails method to make public methods and accessibles
  # metodo do rails para tornar metodos publicos e acessiveis
  helper_method :current_user

  # this method raise one exception, preventing the access not authorized
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end

  skip_authorization_check

  check_authorization

end