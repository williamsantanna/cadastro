class UsersController < ApplicationController

  def index
    @users = User.all

    respond_to do |format|
      format.html
      format.json {render json: @users}
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        # o redirect_to foi modificado para mostrar o index (lista de usuários)
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html {redirect_to users_url, notice:'User was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html{redirect_to users_path, notice: 'User was successfully updated.'}
        format.json{head :no_content}
      else
        format.html{render action: "edit"}
        format.json{render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end