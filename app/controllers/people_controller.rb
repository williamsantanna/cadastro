class PeopleController < ApplicationController

  respond_to :html, :js,:xml, :json
# Este foi o primeiro método criado que instancia a classe Person.

  def new
    @person = Person.new
  end

# Este método instancia a classe Person e guarda na variável de instancia todos os
  def index
     @people = Person.all
     respond_to do |format|
       format.html
     end
  end

# Este metodo recebe como parametro o id de person e guarda na variavel de instancia.
# o metodo salva no banco as informações de @person que foram passadas anteriormente, senão  renderiza o new novamente.
  def create
    @person = Person.new(params[:person])
    @person.save
    respond_with @person do |format|
      format.html { redirect_to @person }
    end
  end

# Este método está sendo chamado após o acionamento do botão "Click Person", a variavel de instancia está recebendo
# o id de person
  def show
    @person = Person.find(params[:id])
  end

  def destroy
    @person = Person.find(params[:id])# recebe como parametros as informações de person.
    @person.destroy #deleta as informações de person do banco.

    respond_to do |format|
      format.html {redirect_to people_path, notice: 'Person was successfully destroyed.'}
      format.json { head :no_content }
    end
  end

  def edit
    @person = Person.find(params[:id])
  end

  def update
    @person = Person.find(params[:id])
    respond_to do |format|
      if @person.update_attributes(params[:person]) #metodo que atualiza person
          format.html {redirect_to person_path} #redireciona para o show
      else
          format.html { render action: "edit" } #renderiza o edit
      end
    end
  end
end
